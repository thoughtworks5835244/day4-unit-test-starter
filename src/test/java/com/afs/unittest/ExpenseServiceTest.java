package com.afs.unittest;

import com.afs.unittest.exception.ProjectExpiredException;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ExpenseServiceTest {

    @InjectMocks
    ExpenseService expenseService;

    @Mock
    ProjectClient projectClient;

    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project internalProject = new Project(ProjectType.INTERNAL, "project name");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(internalProject);

        // then
        assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE, actual);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project externalProject = new Project(ProjectType.EXTERNAL, "Project A");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(externalProject);

        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_A, actual);
    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project externalProject = new Project(ProjectType.EXTERNAL, "Project B");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(externalProject);

        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_B, actual);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project externalProject = new Project(ProjectType.EXTERNAL, "Project Whatever");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(externalProject);

        // then
        assertEquals(ExpenseType.OTHER_EXPENSE, actual);
    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project unexpectedProject = new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "Project Unexpected");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        Throwable actual = assertThrows(
            UnexpectedProjectTypeException.class,
            () -> expenseService.getExpenseCodeByProject(unexpectedProject)
        );

        assertEquals("You enter invalid project type", actual.getMessage());
    }

    @Test
    void should_return_submit_state_when_submit_expense_given_project_do_not_expired() {
        // given
        Project externalProject = new Project(ProjectType.EXTERNAL, "Project X");
        when(projectClient.isExpired(any(Project.class))).thenReturn(false);

        // when
        ExpenseState actual = expenseService.submitExpense(externalProject);

        // then
        assertEquals(ExpenseState.SUBMIT, actual);
    }

    @Test
    void should_throw_expired_exception_when_submit_expense_given_project_has_expired() {
        // given
        Project externalProject = new Project(ProjectType.EXTERNAL, "Project Y");
        when(projectClient.isExpired(any(Project.class))).thenReturn(true);

        // when
        Throwable actual = assertThrows(
            ProjectExpiredException.class,
            () -> expenseService.submitExpense(externalProject)
        );

        // then
        assertNull(actual.getMessage());
    }
}
