package com.afs.unittest;

import com.afs.unittest.exception.ProjectExpiredException;
import com.afs.unittest.exception.UnexpectedProjectTypeException;

import static com.afs.unittest.ExpenseType.*;

class ExpenseService {

    private IProjectClient projectClient;

    ExpenseService(IProjectClient projectClient) {
        this.projectClient = projectClient;
    }

    ExpenseState submitExpense(Project project) {
        if (projectClient.isExpired(project)) {
            throw new ProjectExpiredException();
        }
        return ExpenseState.SUBMIT;
    }

    ExpenseType getExpenseCodeByProject(Project project) throws UnexpectedProjectTypeException {
        ProjectType projectType = project.getProjectType();
        if (!projectClient.isProjectTypeValid(projectType)) {
            throw new UnexpectedProjectTypeException("You enter invalid project type");
        }
        if (projectType == ProjectType.INTERNAL) {
            return INTERNAL_PROJECT_EXPENSE;
        }
        if (projectType == ProjectType.EXTERNAL) {
            String projectName = project.getProjectName();
            if (projectName.equals("Project A")) {
                return EXPENSE_TYPE_A;
            }
            if (projectName.equals("Project B")) {
                return EXPENSE_TYPE_B;
            }
        }
        return OTHER_EXPENSE;
    }
}
